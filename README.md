# Detroit Become Human

Save Game : `%SystemDrive%\Users\Public\Documents\Steam\CODEX\1222140`

**XBOX 360 Controller Emulator**
```
Software Version : 3.2.10.82(2018-07-07) 64-bit
Hook Mask        : COM, 
                   PIDVID,
                   LL (Load Library), 
                   SA (SetupAPI), 
                   WT (WinVerifyTrust)
XInput Files     : 64-bit v1.1, 
                   64-bit v1.2, 
                   64-bit v1.3,
                   64-bit v1.4,
                   64-bit v9.1
DInput File      : 64-bit 
DInput File      : asiloader.dll
Fake VID         : 1118 (0x045E)
Fake PID         : 654 (0x028E)
Timeout          : -1
```

![360xbox](detroit_become_human.PNG)
